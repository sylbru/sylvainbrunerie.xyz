module Card exposing (details)


type alias CardDetails =
    { name : String, job : String, tel : String, email : String }


details : CardDetails
details =
    { name = "Sylvain Brunerie"
    , job = "développeur web | musicien"
    , tel = "+33 (0) 6 08 73 38 39"
    , email = "sylvain.brunerie@gmail.com"
    }
