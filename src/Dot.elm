module Dot exposing (Dot, checkCollisions, init, move, random, rotate, setRotationSpeed, update, view)

import Color exposing (Color)
import Css
import Random
import Svg.Styled as Svg exposing (Svg)
import Svg.Styled.Attributes as SvgAttr


type alias Dot =
    { x : Float
    , y : Float
    , direction : Float
    , speed : Float
    , rotationSpeed : Float
    , color : Color
    }


init : Float -> Float -> Float -> Color -> Dot
init x y direction color =
    { x = x
    , y = y
    , speed = 0.5
    , direction = direction
    , rotationSpeed = -0.1
    , color = color
    }


update : Float -> Dot -> Dot
update delta dot =
    dot
        |> rotate delta
        |> move delta
        |> checkCollisions


setRotationSpeed : Float -> Dot -> Dot
setRotationSpeed angularSpeed dot =
    { dot | rotationSpeed = angularSpeed }


rotate : Float -> Dot -> Dot
rotate delta dot =
    let
        angle =
            (dot.rotationSpeed / 1000) * delta
    in
    { dot | direction = dot.direction + angle }


move : Float -> Dot -> Dot
move delta dot =
    let
        distance =
            (dot.speed / 10000) * delta

        dx =
            cos dot.direction * distance

        dy =
            sin dot.direction * distance

        newX =
            dot.x + dx

        newY =
            dot.y + dy
    in
    { dot
        | x = modFloatBy 1 newX
        , y = modFloatBy 1 newY
    }


modFloatBy : Int -> Float -> Float
modFloatBy div x =
    let
        intPart =
            truncate x

        floatPart =
            x - toFloat intPart
    in
    toFloat (modBy div intPart) + floatPart


checkCollisions : Dot -> Dot
checkCollisions dot =
    let
        checkCollisionsX dot_ =
            if dot_.x > 1 + radius || dot_.x < 0 - radius then
                { dot_ | direction = pi - dot_.direction }

            else
                dot_

        checkCollisionsY dot_ =
            if dot_.y > 1 + radius || dot_.y < 0 - radius then
                { dot_ | direction = negate dot_.direction }

            else
                dot_
    in
    dot |> checkCollisionsX |> checkCollisionsY


view : Dot -> Svg msg
view dot =
    Svg.circle
        [ SvgAttr.r (String.fromFloat radius)
        , SvgAttr.fill (Color.toCssString dot.color)
        , SvgAttr.transform <| translateDot dot.x dot.y
        ]
        []


radius : Float
radius =
    0.2


translateDot : Float -> Float -> String
translateDot x y =
    "translate(" ++ String.fromFloat x ++ ", " ++ String.fromFloat y ++ ")"


random : Random.Generator (List Dot)
random =
    Random.list 50 <|
        Random.map4
            init
            (Random.float 0 1)
            (Random.float 0 1)
            (Random.float 0 (2 * pi))
            randomColor


randomColor : Random.Generator Color
randomColor =
    Random.map3
        Color.hsl
        (Random.float 0.3 0.5)
        (Random.float 0.25 0.75)
        (Random.float 0.1 0.75)
