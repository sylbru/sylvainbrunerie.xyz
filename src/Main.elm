module Main exposing (..)

import Browser
import Browser.Events exposing (Visibility(..))
import Card
import Css
import Dot exposing (Dot)
import Html
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (css)
import Random
import String
import Svg.Styled as Svg
import Svg.Styled.Attributes as SvgAttr
import Time


type alias Model =
    { dots : List Dot
    , visibility : Visibility
    }


type Msg
    = GotFrame Float
    | SetDots (List Dot)
    | ChangeRotationSpeed Time.Posix
    | GotRotationSpeeds (List Float)
    | VisibilityChanged Visibility


init : () -> ( Model, Cmd Msg )
init _ =
    ( { dots = [], visibility = Visible }
    , Dot.random |> Random.generate SetDots
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetDots dots ->
            ( { model | dots = dots }, Cmd.none )

        GotFrame delta ->
            ( { model
                | dots =
                    case model.visibility of
                        Visible ->
                            List.map (Dot.update delta) model.dots

                        Hidden ->
                            model.dots
              }
            , Cmd.none
            )

        GotRotationSpeeds angularSpeeds ->
            ( { model | dots = model.dots |> List.map2 Dot.setRotationSpeed angularSpeeds }, Cmd.none )

        ChangeRotationSpeed _ ->
            ( model
            , Random.float -0.25 0.25
                |> Random.list (List.length model.dots)
                |> Random.generate GotRotationSpeeds
            )

        VisibilityChanged visibility ->
            ( { model | visibility = visibility }, Cmd.none )


view : Model -> List (Html Msg)
view model =
    [ div
        [ css
            [ Css.backgroundColor (Css.hex "#333")
            , Css.color (Css.hex "#fff")
            , Css.height (Css.pct 100)
            , Css.displayFlex
            , Css.alignItems Css.center
            , Css.justifyContent Css.center
            , Css.position Css.relative
            , Css.overflow Css.hidden
            ]
        ]
        [ background model, card ]
    ]


card : Html Msg
card =
    let
        liStyle =
            css
                [ Css.listStyle Css.none
                , Css.marginBottom (Css.em 1)
                ]

        details =
            Card.details
    in
    div
        [ css
            [ Css.fontFamily Css.monospace
            , Css.fontSize (Css.em 1.25)
            , Css.zIndex (Css.int 1)
            , Css.textShadow4 Css.zero Css.zero (Css.em 0.2) (Css.hex "#000")
            , Css.padding (Css.em 3)
            , Css.backgroundColor (Css.rgba 0 0 0 0.2)
            , Css.borderRadius (Css.em 0.5)
            ]
        ]
        [ h1 [ css [ Css.margin Css.zero ] ] [ text details.name ]
        , h2 [] [ text details.job ]
        , ul [ css [ Css.padding Css.zero, Css.margin Css.zero ] ]
            [ li [ liStyle ] [ text details.tel ]
            , li [ liStyle, css [ Css.margin Css.zero ] ] [ text details.email ]
            ]
        ]


background : Model -> Html Msg
background model =
    div
        [ css
            [ Css.position Css.absolute
            , Css.top Css.zero
            , Css.left Css.zero
            , Css.width (Css.pct 100)
            , Css.height (Css.pct 100)
            , Css.zIndex (Css.int 0)
            , Css.property "filter" "blur(3em)"
            ]
        ]
        [ Svg.svg
            [ SvgAttr.viewBox "0 0 1 1", SvgAttr.preserveAspectRatio "none", SvgAttr.width "100%", SvgAttr.height "100%" ]
            (List.map Dot.view model.dots)
        ]


main : Program () Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , view =
            \model ->
                { title = "Sylvain Brunerie"
                , body = List.map toUnstyled (view model)
                }
        , subscriptions =
            \_ ->
                Sub.batch
                    [ Browser.Events.onAnimationFrameDelta GotFrame
                    , Browser.Events.onVisibilityChange VisibilityChanged
                    , Time.every 1000 ChangeRotationSpeed
                    ]
        }
